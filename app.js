var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('dotenv').config();
const mongoose= require('mongoose');


var postsRouter = require('./routes/posts');
var usersRouter = require('./routes/users');

var app = express();

mongoose.connect(`mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/technews`, {useNewUrlParser: true,useUnifiedTopology:true})
mongoose.Promise=global.Promise;

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));
app.use('/posts', postsRouter);
app.use('/users', usersRouter);

app.use((err,req,res,next)=>{
    if (typeof (err) === 'string') {
        // custom application error
        return res.status(400).json({ message: err });
    }

    if (err.name === 'UnauthorizedError') {
        // jwt authentication error
        return res.status(401).json({ message: err.message });
    }

    // default to 500 server error
    return res.status(500).json({ message: err.message });
})
module.exports = app;
