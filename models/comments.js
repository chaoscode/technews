const mongoose= require('mongoose')
const Schema=mongoose.Schema

const CommentsSchema= new Schema({
    author:{
        type:String,
        required:true
    },
    body:{
        type:String,
        required:true
    },
    postId:{
        type:Schema.Types.ObjectID,
        required:true
    },
    createdAt:{
        type:Date,
        default:Date.now()
    }
})
const Comments=mongoose.model('comments',CommentsSchema)
module.exports=Comments
