const mongoose= require('mongoose');
const Schema=mongoose.Schema;

const UsersSchema= new Schema({
    username:{
        type:String,
        required:true,
        unique:true
    },
    password:{
        type:String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique:true
    },
    sex:{
        type: Boolean,
        required:false
    },
    role:{
        type:String,
        default:"user"
    }
});
const Users=mongoose.model('users',UsersSchema);
module.exports=Users;
