const mongoose= require('mongoose')
const Schema=mongoose.Schema

const PostsSchema= new Schema({
   title:{
       type:String,
       required:true
   },
    subtitle:{
       type:String,
        default:""
    },
    author:{
      type:String,
      required:true
    },
    body:{
       type:String,
        required:true
    },
    comments:{
        type:Array,
        default:[]
    },
    commentsCount:{
       type:Number,
        default : 0
    },
    commentsPerPage:{
       type:Number,
        default:40
    },
    createdAt:{
       type:Date,
        default: Date.now()
    },
    modifiedAt:{
       type:Date,
        default:Date.now()
    },
    tags:{
       type:Array,
        default:[]
    },
    image:{
       type:String
   }
})
const Posts=mongoose.model('posts',PostsSchema)
module.exports=Posts
