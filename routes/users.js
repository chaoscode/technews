var express = require('express');
var router = express.Router();
const UsersModel=require('../models/users');
const bcrypt = require('bcrypt');
const saltRounds = parseInt(process.env.PASSWORD_SALT_ROUNDS);
const jwt = require('jsonwebtoken');
const secret = process.env.JWT_SECRET;
const roles= require('../utils/roles');
const authorize=require('../utils/authorize');

/* GET users listing. */
router.get('/',authorize(roles.admin),async function(req, res, next) {
   UsersModel.find().exec().then((users)=>{
     res.send(users)
   }).catch(next)
});

router.get('/:id',async function (req,res,next) {
    UsersModel.findById(req.params.id).exec().then((user)=>{
        res.send(user)
    }).catch(next)
});

router.post('/',async function (req,res,next) {
    let newUser=req.body;
    bcrypt.genSalt(saltRounds, function(err, salt) {
        bcrypt.hash(newUser.password, salt, function(err, hash) {
            newUser.password=hash
            newUser.role=roles.user;

            UsersModel.create(newUser).then( (user) =>{
                    //console.log(user)
                    user.password="";
                    res.send(user)
                }
            ).catch(next)
        });
    });
});
router.post('/admin',authorize(roles.admin),async function (req,res,next) {
    let newUser=req.body;
    bcrypt.genSalt(saltRounds, function(err, salt) {
        bcrypt.hash(newUser.password, salt, function(err, hash) {
            newUser.password=hash

            UsersModel.create(newUser).then( (user) =>{
                    //console.log(user)
                    user.password="";
                    res.send(user)
                }
            ).catch(next)
        });
    });
});


router.post('/login',async function (req,res,next) {
    UsersModel.findOne( { username: req.body.username} ).exec().then((user)=>{
        if (user) {
            //console.log(JSON.stringify(user));
            //console.log(req.body.password + " "+ user.password);
                    bcrypt.compare(req.body.password, user.password, function (err, result) {
                        if (err || result===false) {
                           next(Error("wrong password"))
                        }
                        if (result == true) {
                            res.send({
                                username: user.username,
                                token: jwt.sign({id: user._id, role: user.role , username: user.username}, secret)
                            })
                        }
                    });
        }else{
            res.status(404).send(Error("User Not Found"))
        }
    }).catch(next);

});

module.exports = router;
