var express = require('express');
var router = express.Router();
const PostModel=require('../models/posts');
const CommentModel=require('../models/comments');
const roles= require('../utils/roles');
const authorize=require('../utils/authorize');

/* GET home page. */
router.get('/', function(req, res, next) {
  PostModel.find().exec().then((posts)=>{
    res.send(posts.sort((a,b)=>{
      return new Date(a.createdAt) - new Date(b.createdAt)
    }).reverse())
  }).catch(next)
});

router.post('/',authorize([roles.admin,roles.editor]),function (req,res,next) {
  PostModel.create(req.body).then((post)=>{
    res.send(post)
  })
});

router.get('/:title',function (req,res,next) {
  PostModel.find({title:req.params.title}).exec().then((posts)=>{
    res.send(posts)
  }).catch(next)
});
router.get('/byid/:id',function (req,res,next) {
  console.log(req.params.id)
  PostModel.findById(req.params.id).exec().then((posts)=>{
    res.send(posts)
  }).catch(next)
});
router.patch('/:id',authorize([roles.admin,roles.editor]), async function (req,res,next) {
  PostModel.findById(req.params.id).exec().then((post)=>{
    PostModel.updateOne({ _id:post.id},req.body).then((post)=>{
      res.send({message:"post updated"})
    }).catch(next)
  })
});
router.delete('/:id',authorize([roles.admin]), async function (req,res,next) {
  PostModel.deleteOne({_id:req.params.id}).then((err)=>{
    res.send({message:"post deleted"})
  }).catch(next)
});
router.get('/:id/comments',async function (req,res,next) {
  CommentModel.find({postId:req.params.id}).exec().then((comments)=>{
    res.send(comments.sort((a,b)=>{
      return new Date(a.createdAt) - new Date(b.createdAt)
    }).reverse()
    )
  }).catch(next)
});

router.post('/:id/comments',authorize(),async  function (req,res,next) {
  PostModel.findById(req.params.id).exec().then((post)=> {
    let comments=[]
    comments=post.comments.sort((a,b)=>{
      return new Date(a.createdAt) - new Date(b.createdAt)
    }).reverse();
    if(post.commentsCount >= post.commentsPerPage){
      comments.pop();
    }
    comments.push(req.body)
    comments=comments.sort((a,b)=>{
      return new Date(a.createdAt) - new Date(b.createdAt)
    }).reverse();
    post.comments=comments;
    post.commentsCount++;
    PostModel.updateOne({_id:post._id},post).exec().then((err)=>{})
      }).catch(next)
  CommentModel.create(req.body).then((comment)=>{
    res.send(comment)
  });
});

router.delete('/:id/comments/:commentid', authorize(roles.admin) , async function (req,res,next) {
  {
  CommentModel.deleteOne({_id:req.params.commentid}).then( (err)=>{
      res.send({message:"comment deleted"})
  }).catch(next)
}
});

module.exports = router;
